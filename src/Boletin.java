public class Boletin {
    private Alumnos alumno;
    private Materias materia;
    private double[] notas = new double[12];

    public Boletin(Alumnos alumno, Materias materia, double[] notas) {
        this.alumno = alumno;
        this.materia = materia;
        this.notas = notas;
    }

    public Boletin(Alumnos alumno, double nota1, double nota2, double nota3, double nota4, double nota5, double nota6, double nota7, double nota8, double nota9, double nota10, double nota11, double nota12) {
        this.alumno = alumno;
        this.notas[0] = nota1;
        this.notas[1] = nota2;
        this.notas[2] = nota3;
        this.notas[3] = nota4;
        this.notas[4] = nota5;
        this.notas[5] = nota6;
        this.notas[6] = nota7;
        this.notas[7] = nota8;
        this.notas[8] = nota9;
        this.notas[9] = nota10;
        this.notas[10] = nota11;
        this.notas[11] = nota12;

    }

    public void imprimeBoletin(){
        System.out.println("Alumno: " + alumno);
        System.out.println(" ");
        System.out.println(alumno.getNombre() + " " + alumno.getApellidos());
        System.out.println(" ");
        System.out.println(alumno.getEmilio());
        System.out.println(" ");

        System.out.println("Materia" + '\t' + '\t' +  "Descripción" + "\t" + "\t" + "\t" + "\t" + "\t" + "Docente" + "\t" + "\t" + "\t" + "\t" + "Calificacón");
        System.out.println(Materias.CJ001 + "\t" + "\t" + Materias.CJ001.getDescripcion() + "\t" + "\t" + "\t"  + "\t" + Materias.CJ001.getDocente() + "\t" + "\t" + "\t" + calificacion(notas[0]));
        System.out.println(Materias.CJ002 + "\t" + "\t" + "\t" + Materias.CJ002.getDescripcion() + "\t" + "\t" + "\t" + "\t" + "\t" + Materias.CJ002.getDocente() + "\t" + "\t" + "\t" + calificacion(notas[1]));
        System.out.println(Materias.CJ003 + "\t" + "\t" + Materias.CJ003.getDescripcion() + "\t" + "\t" + "\t" + Materias.CJ003.getDocente() + "\t" + calificacion(notas[2]));
        System.out.println(Materias.CJ004 + "\t" + "\t" + Materias.CJ004.getDescripcion() + "\t" + "\t" + "\t" + Materias.CJ004.getDocente() + "\t" + "\t" + "\t" + calificacion(notas[3]));
        System.out.println(Materias.CJ005 + "\t" + "\t" + Materias.CJ005.getDescripcion() + "\t" + "\t" + Materias.CJ005.getDocente() + "\t" + calificacion(notas[4]));
        System.out.println(Materias.CJ006 + "\t" + "\t" + Materias.CJ006.getDescripcion() + "\t" + "\t" + Materias.CJ006.getDocente() + "\t" + "\t" + "\t" + calificacion(notas[5]));
        System.out.println(Materias.CJ007 + "\t" + "\t" + "\t" + Materias.CJ007.getDescripcion() + "\t" + "\t" + "\t" + Materias.CJ007.getDocente() + "\t" + "\t" + "\t" + calificacion(notas[6]));
        System.out.println(Materias.CJ008 + "\t" + "\t" + Materias.CJ008.getDescripcion() + "\t" + "\t" + "\t" + "\t" + Materias.CJ008.getDocente() + "\t" + "\t" + calificacion(notas[7]));
        System.out.println(Materias.CJ009 + "\t" + "\t" + Materias.CJ009.getDescripcion() + "\t" + "\t" + "\t" + Materias.CJ009.getDocente() + "\t" + "\t" + calificacion(notas[8]));
        System.out.println(Materias.CJ010 + "\t" + "\t" + Materias.CJ010.getDescripcion() + "\t" + "\t" + "\t" + Materias.CJ010.getDocente() + "\t" + "\t" + "\t" + "\t" + calificacion(notas[9]));
        System.out.println(Materias.CJ011 + "\t" + "\t" + Materias.CJ011.getDescripcion() + "\t" + "\t" + "\t" + "\t" + Materias.CJ011.getDocente() + "\t" + "\t" + calificacion(notas[10]));
        System.out.println(Materias.CJ012 + "\t" + "\t" + "\t" + Materias.CJ012.getDescripcion() + "\t" + "\t" + "\t" + "\t" + Materias.CJ012.getDocente() + "\t" + "\t" + calificacion(notas[11]));

        System.out.println(" ");

        double media = (notas[0] + notas[1] + notas[2] + notas[3] + notas[4] + notas[5]+ notas[6]+ notas[7] + notas[8] + notas[9] + notas[10] + notas[11])/12;
        System.out.println("Calificación media en el curso: " + calificacion(media));

        System.out.println(" ");
        System.out.println("Nota media con Eduardo Corral: " + calificacion((notas[0] + notas[1])/2 ));
        System.out.println("Nota media con Carlos Ponce de León: " + calificacion((notas[2] + notas[4])/2 ));
        System.out.println("Nota media con Diego Avendano: " + calificacion((notas[5] + notas[6])/2 ));
        System.out.println("Nota media con José Manuel Aroca: " + calificacion((notas[7] + notas[8])/2 ));
        System.out.println(" ");

        firma();


    }

    public static String calificacion(double nota){

        if(nota < 5){
            return "Insuficiente - " + nota;
        }
        else if(nota >= 5 && nota < 6){
            return "Suficiente - " + nota;
        }
        else if(nota >=6 && nota < 7){
            return "Bien - " + nota;
        }
        else if(nota >= 7 && nota < 9){
            return "Notable - " + nota;
        }
        else if(nota >= 9 && nota <= 10){
            return "Sobresaliente - " + nota;
        }
        else{
            return "Nota desconocida";
        }

    }

    public static void firma(){
        System.out.print("Documento firmado electronicamente: " );
        generaClave();
        System.out.println(" ");

    }
    public static void generaClave(){

            String banco = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

            char[] caracter = banco.toCharArray();
            int repeticiones = 0;

            do{
                int indice = (int)(Math.random() * banco.length());
                System.out.print(caracter[indice]);
                repeticiones++;

            }while(repeticiones < 20);

    }





}
