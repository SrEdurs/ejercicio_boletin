public enum Alumnos {
    JN2001(1,"Africa","Reinoso Franco","africa_r@javaeoi.es"),
    JN2002(2,"Alejandro","Cazalla Cimadom","alejandro_c@javaeoi.es"),
    JN2003(3,"Alejandro","Gallego Domínguez","alejandro_g@javaeoi.es"),
    JN2004(4,"Alfonso Jesus","Anillo Romero","alfonso_jesus_a@javaeoi.es"),
    JN2005(5,"Ana Maria","Visiga Hernández","ana_maria_v@javaeoi.es"),
    JN2006(6,"Anastasia","Fernández de Castro Sanchez","anastasia_f@javaeoi.es"),
    JN2007(7,"Andres","Tenllado Perez","andres_t@javaeoi.es"),
    JN2008(8,"Beatriz","Bello Sánchez","beatriz_b@javaeoi.es"),
    JN2009(9,"Claudia Marilyn","Ferreira Unzain","claudia_marilyn_f@javaeoi.es"),
    JN2010(10,"Dolores","Rechi Vega","dolores_r@javaeoi.es"),
    JN2011(11,"Eduardo","Rojas Soto","eduardo_r@javaeoi.es"),
    JN2012(12,"Emilio","Senabre López","emilio_s@javaeoi.es"),
    JN2013(13,"Ignacio","Lorca Hinojosa","ignacio_l@javaeoi.es"),
    JN2014(14,"Jhon Ferney","Aristizabal Sánchez","jhon_ferney_a@javaeoi.es"),
    JN2015(15,"Juan","García Borbolla","juan_g@javaeoi.es"),
    JN2016(16,"Maria Isabel","Ortega Soriano","maria_isabel_o@javaeoi.es"),
    JN2017(17,"Natanael Alberto","Eusebio Matos","natanael_alberto_e@javaeoi.es"),
    JN2018(18,"Ramon Jesus","Gómez Carmona","ramon_jesus_g@javaeoi.es"),
    JN2019(19,"Ruddy Eddi","PEREZ SALGUERO","ruddy_eddi_p@javaeoi.es"),
    JN2020(20,"Sara","Jiménez Nares","sara_j@javaeoi.es"),
    JN2021(21,"Sergio","Carrera Martínez","sergio_c@javaeoi.es"),
    JN2022(22,"Sergio","Fernandez Alvarez","sergio_f@javaeoi.es"),
    JN2023(23,"Sergio","López Álvarez","sergio_l@javaeoi.es");

    private int ID;
    private String nombre;
    private String apellidos;
    private String emilio;

    Alumnos(int ID, String nombre, String apellidos, String emilio) {
        this.ID = ID;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.emilio = emilio;
    }
    public String getNombre() {
        return nombre;
    }
    public String getApellidos() {
        return apellidos;
    }
    public String getEmilio() {
        return emilio;
    }
    public static void  listaAlumnos(){

        System.out.println("¡Buenas! selecciona tu usuario para iniciar:");

        System.out.println("1. Africa" + '\n' + "2. Alejandro Cazalla" + '\n' + "3. Alejandro Gallego" +
                '\n' + "4. Alfonso" + '\n' + "5. Ana Maria" + '\n' + "6. Anastasia" + '\n' +
                "7. Andres" + '\n' + "8. Beatriz" + '\n' + "9. Claudia" +  '\n' + "10. Lola " +
                '\n' + "11. Eduardo Rojas" + '\n' + "12. Emilio" + '\n' + "13. Ignacio" + '\n' +
                "14. Jhon" + '\n' + "15. Juan" + '\n' + "16. Isa" + '\n' + "17. Natanael" + '\n' +
                "18. Monchu" + '\n' + "19. Ruddy" + '\n' + "20. Sara" + '\n' + "21. Sergio Carrera"
                + '\n' + "22. Sergio Fernandez"+ '\n' + "23. Sergio Lopez");

    }

}
